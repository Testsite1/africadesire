<?php

namespace App\Components;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Country;

trait RegisterUsers
{
    use RegistersUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $countries = Country::pluck('name', 'id');

        $months = [
            ''  => 'Month',
            1   => 'Jan',
            2   => 'Feb',
            3   => 'Mar',
            4   => 'Apr',
            5   => 'May',
            6   => 'Jun',
            7   => 'Jul',
            8   => 'Aug',
            9   => 'Sep',
            10  => 'Oct',
            11  => 'Sep',
            12  => 'Dec',
        ];

        $days = ['' => 'Day'];
        for ($i=1; $i<=31; $i++) {
            $days[$i] = $i;
        }

        $years = ['' => 'Year'];
        for ($i=1940; $i<=2007; $i++) {
            $years[$i] = $i;
        }

        return view('auth.register', [
            'countries' => $countries,
            'months' => $months,
            'days' => $days,
            'years' => $years,
        ]);
    }
}